# Retours

- Merci Georges

## Expressions / instructions


* expressions  - évaluées
* instructions - exécutées

régles d'évaluations:
* précendences
* remplacement variables par valeurs
* remplacemeent de crochets et virgules par des listes
* remplacement de mot + parenthèse par l'appel
  à une fonction
* évaluation des arguments avant l'appel
* etc ...


## Variables / assignation

* On assigne une valeur à une variable (dans cet ordre) - la valeur
  étant toujours une *expression*


## méthodes et attributs

les méthodes _sont_ des attributs des classes

## flot de controle

change l'ordre d'exécution des instruction : les conditions
if, else, les boucles while/if, et le try: except font
partie du flot de controle

## rappels exceptions

## Discussions

* fin de depydoc - transformé en un autre cours
* que faire pour la suite ?
   * soit une grosse aplli de bureau
   * soit retourner à des ateliers indépendants
