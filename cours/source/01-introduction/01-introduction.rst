Objet de ce livre
=================

Apprendre la programmation en partant de rien, en utilisant Python et la ligne de commande

Pourquoi Python?
----------------

* Excellent langage pour débuter - il a d'ailleurs été créé avec cet objectif en tête
* Mon langage préféré
* Facile à installer quelque soit le système d'exploitation (Linux, macOS, Windows)

Pourquoi la ligne de commande?
------------------------------

C'est un interface universelle. Pour faire des programmes en lignes de commande,
on n'a juste besoin de manipuler du *texte*, et c'est le plus simple au début.
