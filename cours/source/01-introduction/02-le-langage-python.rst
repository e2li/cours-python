Présentation du langage Python
==============================

Utilisation de Python
----------------------

* Aussi appelé "langage de script", `glue language`

* Bon partout, excellent nulle part

* Exemples d'utilisation:

    * Sciences (physique, chimie, linguistique ...)
    * Animation (Pixar, Disney ...)
    * Sites web (journaux, youtube, ...)
    * Ligne de commande
    * ...

Petit détour: version d'un programme
------------------------------------

* Comme les versions d'un document
* Si le nombre est plus grand, c'est plus récent
* Souvent en plusieurs morceaux: `1.3, 1.4, 3.2.5`. etc
* Plus l'écart est grand, plus le programme a changé.
    * `3.2.5 -> 3.2.6`: pas grand-chose
    * `1.5.1 -> 4.3`: beaucoup de changements
* On omet souvent le reste des numéros quand c'est pas nécessaire

Historique
----------

* Créé par Guido van Rossum. Conçu à la base pour l'enseignement.
* Le nom vient des Monty Python (si, si)
* Python 1: Sortie en 1991
* Python 2: en 2000
* Python 3: en 2008

Le grand schisme
----------------

La plupart des langages continuent à être compatibles d'une version à l'autre.

*Ce n'est pas le cas pour Python3*, et ça a causé beaucoup de confusion et de débats.

Heureusement, 10 ans plus tard, la situation s'est arrangée, et Python2 cessera d'être maintenu le premier janvier 2020.

Python3
-------

Ce cours fonctionne donc uniquement avec Python3.

N'utilisez *pas* Python2, sinon certaines choses expliquées ici ne marcheront pas :/
