Introduction aux fonctions
==========================

.. toctree::
   :maxdepth: 1

   01-functions.rst
   02-portée-des-variables.rst
   03-plusieurs-arguments.rst
   04-par-défaut.rst
   05-fonctions-natives.rst
   06-return.rst
