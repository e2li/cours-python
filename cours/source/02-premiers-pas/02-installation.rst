Installation
============

Pour suivre ce cours, il vous faudra installer deux composants
essentiels:

* L'interpréteur Python
* Un éditeur de texte

Installation de Python
----------------------

Linux
+++++

Il y a toutes les chances que Python soit déjà installé sur votre
distribution. Pour vous en assurer, tapez:

.. code-block:: console

   python3 --version

Sinon, installez le paquet correspondant à Python3 (quelque chose comme
``sudo apt install python3```


macOS
++++++

Python3 est disponible dans `homebrew <https://brew.sh/>`_

.. code-block:: console

   brew install python


Windows
+++++++

Python3 est disponible dans Microsoft store.

Sinon, vous pouvez aussi télécharger l'installeur depuis https://python.org

Veillez à cocher la case "Ajouter Python au PATH"


Installation d'un éditeur de texte
----------------------------------

Je vous conseille pour commencer d'utiliser un éditeur
de texte basique. Vous n'avez pas besoin d'un IDE,
surtout si vous débutez

* Linux; ``gedit``, ``kate``, ...
* macOS: ``CotEditor``
* Windows: ``Notepad++``

J'insiste sur **simple**. Inutile d'installer un IDE pour le moment.


Configuration de l'éditeur de texte
------------------------------------

Prenez le tempe de configurer votre éditeur
de texte de la façon suivante:

* Police de caractères à chasse fixe
* Indentation de *4 espaces*
* Remplacer les tabulations par des espaces
* Activer la coloration syntaxique

Cela vous évitera des soucis plus tard.
