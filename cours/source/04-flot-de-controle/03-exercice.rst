Exercice
========

// TODO: explication des exercises

Lire une entrée utilisateur
----------------------------

* `input()`  (encore des parenthèses ...)

    * interrompt le script
    * lit ce que l'utilisateur tape jusqu'à ce qu'il tape "entrée".
    * renvoie une string

Le jeu
------

On fait deviner un nombre à l'utilisateur, en affichant 'trop grand', 'trop petit'
jusqu'à ce qu'il trouve la valeur exacte.

Squelette
---------

// TODO:
* explication du Squelette
* pas de solution!

.. code-block:: python

   # faites moi confiance, les deux lignes ci-dessous
   # permettent de tirer un nombre au hasard entre 0 et 100
   import random
   nombre = random.randint(0, 100)

   print("devine le nombre auquel je pense")

   # votre code ici
