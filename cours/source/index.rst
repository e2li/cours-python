Bienvenue
=========

Ce livre contient les supports de cours destinés aux élève de l'École du Logiciel Libre,
où je donne des cours sur Python depuis 2018.

Il est destiné à toute personne qui voudrait découvrir la programmation.

J'espère qu'il vous plaira! Sinon, vous pouvez vous rendre sur `ma page de
contact <https://dmerej.info/blog/fr/pages/about>`_ et me faire part de vos
remarques.

Enfin, notez que ce cours est  placé sous licence `CC BY 4.0 <https://creativecommons.org/licenses/by/4.0/deed.fr>`_. Bonne lecture!

.. toctree::
   :maxdepth: 1
   :caption: Table des matières:

   01-introduction/index.rst
   02-premiers-pas/index.rst
   03-variables-et-types/index.rst
   04-flot-de-controle/index.rst
   05-fonctions-01/index.rst
   06-listes/index.rst
   07-none-et-pass/index.rst
   08-dictionnaires/index.rst
   09-tuples/index.rst
   10-classes-01/index.rst
   11-modules-01/index.rst
   12-classes-02/index.rst
   13-bibliothèques-01/index.rst
   14-fichiers-et-données-binaires/index.rst
   15-interpréteur-interactif/index.rst
   16-classes-03/index.rst
   17-décorateurs/index.rst
   18-exceptions/index.rst
   19-classes-04/index.rst
