Données binaires et fichiers
============================

On vous a peut-être déjà dit que l'informatique consiste à
manipuler des suites de 0 et et de 1s, mais qu'en est-t-il
exactement ?

De manière surprenante, la réponse à cette question
va nous emmener sur un chemin qui va parler
de maths, puis de comment le langage Python
peut interagir avec "l'extérieur".

.. toctree::
   :maxdepth: 1

   01-données-binaires
   02-fichiers
